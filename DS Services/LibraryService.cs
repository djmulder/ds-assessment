﻿using DS_Exceptions;
using DS_Interfaces;
using DS_Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DS_Services
{
    public class LibraryService : ILibraryService
    {
        private ILibraryRepository LibraryRepository { get; }

        public LibraryService(ILibraryRepository libraryRepository)
        {
            // Get library item repo
            LibraryRepository = libraryRepository ?? throw new ArgumentNullException();
        }

        // Create library item
        public async Task<LibraryItem> CreateLibraryItemAsync(LibraryItem libraryItem)
        {
            var createdLibraryItem = await LibraryRepository.CreateLibraryItemAsync(libraryItem);

            return createdLibraryItem;
        }

        // Read One library item
        public async Task<LibraryItem> ReadOneLibraryItemAsync(Guid libraryItemKey)
        {
            var libraryItem = await LibraryRepository.ReadOneLibraryItemAsync(libraryItemKey) ?? throw new LibraryItemNotFoundException();
            
            return libraryItem;
        }

        // Read All library items
        public async Task<IEnumerable<LibraryItem>> ReadAllLibraryItems()
        {
            return await LibraryRepository.ReadAllLibaryItemsAsync();
        }

        // Update library item
        public async Task<LibraryItem> UpdateLibraryItemAsync(LibraryItem libraryItem)
        {
            var remoteLibraryItem = await ReadOneLibraryItemAsync(libraryItem.LibraryItemKey);

            var updatedLibraryItem = await LibraryRepository.UpdateLibraryItemAsync(libraryItem);

            return updatedLibraryItem;

        }

        // Delete library item
        public async Task<LibraryItem> DeleteLibraryItemAsync(Guid libraryKey)
        {
            var remoteLibraryItem = await ReadOneLibraryItemAsync(libraryKey);

            var deletedLibraryItem = await LibraryRepository.DeleteLibraryItemAsync(libraryKey);
            return deletedLibraryItem;
        }

        public async Task<IEnumerable<LibraryItem>> SearchLibrayItemsAsync(string search)
        {
            return await LibraryRepository.SearchLibraryItemsAsync(search);
        }
    }
}
