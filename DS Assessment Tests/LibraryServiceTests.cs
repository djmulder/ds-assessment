﻿using DS_Exceptions;
using DS_Interfaces;
using DS_Model;
using DS_Services;
using Moq;
using Xunit;

namespace DS_Assessment_Tests
{
    public class LibraryServiceTests
    {
        // Setup
        protected Mock<ILibraryRepository> LibraryRepository;
        protected LibraryService LibraryService { get; }

        public LibraryServiceTests()
        {
            LibraryRepository = new Mock<ILibraryRepository>();
            LibraryService = new LibraryService(LibraryRepository.Object);
        }

        // Test Create
        public class CreateAsync : LibraryServiceTests
        {
            [Fact]
            public async void Should_Create_And_Return_LibraryItem()
            {
                // Setup
                var expectedLibraryItem = new LibraryItem();
                LibraryRepository
                    .Setup(lir => lir.CreateLibraryItemAsync(expectedLibraryItem))
                    .ReturnsAsync(expectedLibraryItem)
                    .Verifiable();

                // Get result
                var res = await LibraryService.CreateLibraryItemAsync(expectedLibraryItem);

                // Assert
                Assert.Same(expectedLibraryItem, res);
                LibraryRepository.Verify(lir => lir.CreateLibraryItemAsync(expectedLibraryItem), Times.Once);
            }
        }

        // Test Read One
        public class ReadOneAsync : LibraryServiceTests
        {
            [Fact]
            public async void Should_Return_LibaryItem()
            {
                // Setup 
                var expectedLibraryItem = new LibraryItem { Name = "Test LibraryItem" };

                LibraryRepository
                    .Setup(lir => lir.ReadOneLibraryItemAsync(expectedLibraryItem.LibraryItemKey))
                    .ReturnsAsync(expectedLibraryItem);

                // Get result
                var res = await LibraryService.ReadOneLibraryItemAsync(expectedLibraryItem.LibraryItemKey);

                // Assert
                Assert.Same(expectedLibraryItem, res);
            }

            [Fact]
            public async void Should_Throw_LibraryItemNotFound()
            {
                // Setup
                var dummyLibraryItem = new LibraryItem { Name = "Bogus LibraryItemName" };

                LibraryRepository
                    .Setup(lir => lir.ReadOneLibraryItemAsync(dummyLibraryItem.LibraryItemKey))
                    .ReturnsAsync(default(LibraryItem));

                // Get / Assert
                await Assert.ThrowsAsync<LibraryItemNotFoundException>(() => LibraryService.ReadOneLibraryItemAsync(dummyLibraryItem.LibraryItemKey));
            }
        }

        // Search 
        public class SearchAsync : LibraryServiceTests
        {
            [Fact]
            public async void Should_Return_Found_LibraryItems()
            {
                // Setup 
                var search = "Test";
                var expectedLibraryItem = new LibraryItem { Name = "Test LibraryItem" };

                LibraryRepository
                    .Setup(lir => lir.SearchLibraryItemsAsync(search))
                    .ReturnsAsync(new LibraryItem[] { expectedLibraryItem });

                // Get result
                var res = await LibraryService.SearchLibrayItemsAsync(search);

                // Assert
                Assert.Collection(res,
                    li => Assert.Same(expectedLibraryItem, li)
                );
            }

            [Fact]
            public async void Should_Not_Return_LibraryItems()
            {
                // Setup
                var bogusSearch = "Bla";

                LibraryRepository
                    .Setup(lir => lir.SearchLibraryItemsAsync(bogusSearch))
                    .ReturnsAsync(default(LibraryItem[]));

                // Get result
                var res = await LibraryService.SearchLibrayItemsAsync(bogusSearch);

                // Asset
                Assert.Null(res);
            }
        }


        // Test Read All
        public class ReadAllAsync : LibraryServiceTests
        {
            [Fact]
            public async void Should_Return_All_LibraryItems()
            {
                // Setup
                var expectedLibraryItems = new LibraryItem[]
                {
                    new LibraryItem { Name = "Test LibraryItem 1" },
                    new LibraryItem { Name = "Test LibraryItem 2" },
                    new LibraryItem { Name = "Test LibraryItem 3" }
                };

                LibraryRepository
                    .Setup(lir => lir.ReadAllLibaryItemsAsync())
                    .ReturnsAsync(expectedLibraryItems);

                // Get result
                var res = await LibraryService.ReadAllLibraryItems();

                // Assert
                Assert.Same(expectedLibraryItems, res);
            }
        }

        // Test Update
        public class UpdateAsync : LibraryServiceTests
        {
            [Fact]
            public async void Should_Update_LibraryItem()
            {
                // Setup
                var expectedLibraryItem = new LibraryItem { Name = "Test LibraryItem 1" };

                LibraryRepository
                    .Setup(lir => lir.ReadOneLibraryItemAsync(expectedLibraryItem.LibraryItemKey))
                    .ReturnsAsync(expectedLibraryItem);
                LibraryRepository
                    .Setup(lir => lir.UpdateLibraryItemAsync(expectedLibraryItem))
                    .ReturnsAsync(expectedLibraryItem)
                    .Verifiable();

                // Get result
                var res = await LibraryService.UpdateLibraryItemAsync(expectedLibraryItem);

                // Assert
                Assert.Same(expectedLibraryItem, res);
                LibraryRepository.Verify(lir => lir.UpdateLibraryItemAsync(expectedLibraryItem), Times.Once);
            }

            [Fact]
            public async void Should_Throw_LibraryItemNotFoundException()
            {
                // Setup
                var dummyLibraryItem = new LibraryItem { Name = "Bogus LibraryItemName" };
                LibraryRepository
                    .Setup(lir => lir.UpdateLibraryItemAsync(dummyLibraryItem))
                    .Verifiable();

                LibraryRepository
                    .Setup(lir => lir.ReadOneLibraryItemAsync(dummyLibraryItem.LibraryItemKey))
                    .ReturnsAsync(default(LibraryItem))
                    .Verifiable();

                // Get result / Assert
                await Assert.ThrowsAsync<LibraryItemNotFoundException>(() => LibraryService.UpdateLibraryItemAsync(dummyLibraryItem));

                LibraryRepository
                    .Verify(lir => lir.UpdateLibraryItemAsync(dummyLibraryItem), Times.Never);

                LibraryRepository
                    .Verify(lir => lir.ReadOneLibraryItemAsync(dummyLibraryItem.LibraryItemKey), Times.Once);
            }

        }

        // TODO: Delete
        public class DeleteAsync : LibraryServiceTests
        {
            [Fact]
            public async void Should_Delete_LibraryItem()
            {
                // Setup
                var expectedLibraryItem = new LibraryItem { Name = "Test LibraryItem 1" };

                LibraryRepository
                    .Setup(lir => lir.ReadOneLibraryItemAsync(expectedLibraryItem.LibraryItemKey))
                    .ReturnsAsync(expectedLibraryItem);
                LibraryRepository
                    .Setup(lir => lir.DeleteLibraryItemAsync(expectedLibraryItem.LibraryItemKey))
                    .ReturnsAsync(expectedLibraryItem)
                    .Verifiable();

                // Get result
                var res = await LibraryService.DeleteLibraryItemAsync(expectedLibraryItem.LibraryItemKey);

                // Assert
                Assert.Same(expectedLibraryItem, res);
                LibraryRepository
                    .Verify(lir => lir.DeleteLibraryItemAsync(expectedLibraryItem.LibraryItemKey), Times.Once);
            }

            [Fact]
            public async void Should_Throw_LibraryItemNotFound()
            {
                // Setup
                var dummyLibraryItem = new LibraryItem { Name = "Bogus LibraryItemName" };
                LibraryRepository
                    .Setup(lir => lir.DeleteLibraryItemAsync(dummyLibraryItem.LibraryItemKey))
                    .Verifiable();

                LibraryRepository
                    .Setup(lir => lir.ReadOneLibraryItemAsync(dummyLibraryItem.LibraryItemKey))
                    .ReturnsAsync(default(LibraryItem))
                    .Verifiable();

                // Get result / Assert
                await Assert.ThrowsAsync<LibraryItemNotFoundException>(() => LibraryService.DeleteLibraryItemAsync(dummyLibraryItem.LibraryItemKey));

                LibraryRepository
                    .Verify(lir => lir.DeleteLibraryItemAsync(dummyLibraryItem.LibraryItemKey), Times.Never);

                LibraryRepository
                    .Verify(lir => lir.ReadOneLibraryItemAsync(dummyLibraryItem.LibraryItemKey), Times.Once);
            }
        }
    }
}
