﻿using DS_Contexts;
using DS_Exceptions;
using DS_Interfaces;
using DS_Model;
using DS_Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace DS_Assessment_Tests
{
    public class LibraryRepositoryTests
    {
        // Setup
        protected LibraryRepository LibraryRepository { get; }
        protected Mock<LibraryContext> LibraryContext { get; }

        public LibraryRepositoryTests()
        {
            var libraryItems = new LibraryItem[]
            {
                new LibraryItem { Name = "Test LibraryItem 1" },
                new LibraryItem { Name = "Test LibraryItem 2" },
                new LibraryItem { Name = "Test LibraryItem 3" }
            }.AsQueryable();


            var libraryItemSet = new Mock<DbSet<LibraryItem>>();
            libraryItemSet.As<IQueryable<LibraryItem>>().Setup(m => m.Provider).Returns(libraryItems.Provider);
            libraryItemSet.As<IQueryable<LibraryItem>>().Setup(m => m.Expression).Returns(libraryItems.Expression);
            libraryItemSet.As<IQueryable<LibraryItem>>().Setup(m => m.ElementType).Returns(libraryItems.ElementType);
            libraryItemSet.As<IQueryable<LibraryItem>>().Setup(m => m.GetEnumerator()).Returns(libraryItems.GetEnumerator());

            LibraryContext = new Mock<LibraryContext>();
            LibraryContext.Setup(lc => lc.LibraryItems).Returns(libraryItemSet.Object);

            LibraryRepository = new LibraryRepository(LibraryContext.Object);
        }

        // Test Create
        public class CreateAsync : LibraryRepositoryTests
        {
            [Fact]
            public async void Should_Create_And_Return_LibraryItem()
            {
                // Setup
                var expectedItem = new LibraryItem { Name = "New Test LibraryItem" };
                LibraryContext.Setup(lc => lc.LibraryItems.Add(expectedItem)).Verifiable();

                // Get result
                var res = await LibraryRepository.CreateLibraryItemAsync(expectedItem);

                // Assert
                Assert.Same(expectedItem, res);
                LibraryContext.Verify(lc => lc.SaveChanges(), Times.Once);
            }
        }

        // Test Read One
        public class ReadOneAsync : LibraryRepositoryTests
        {
            [Fact]
            public async void Should_Return_LibaryItem()
            {
                // Setup
                var expectedlibraryItem = LibraryContext.Object.LibraryItems.First();

                // Get result
                var res = await LibraryRepository.ReadOneLibraryItemAsync(expectedlibraryItem.LibraryItemKey);

                // Assert
                Assert.Same(expectedlibraryItem, res);
            }

            [Fact]
            public async void Should_Return_Null()
            {
                // Setup
                var bogusLibraryKey = Guid.NewGuid();

                // Get result
                var res = await LibraryRepository.ReadOneLibraryItemAsync(bogusLibraryKey);

                // Assert
                Assert.Null(res);
            }
        }

        public class SearchAsync : LibraryRepositoryTests
        {
            [Fact]
            public async void Should_Return_Found_LibraryItems()
            {
                // Setup 
                var search = "Test";
                var expectedLibraryItems = LibraryContext.Object.LibraryItems.Where(li => search.Contains(li.Name, StringComparison.InvariantCultureIgnoreCase)).ToArray();

                // Get result
                var res = await LibraryRepository.SearchLibraryItemsAsync(search);

                // Assert
                Assert.True(expectedLibraryItems.All(shouldItem => res.Any(isItem => isItem == shouldItem)));
            }

            [Fact]
            public async void Should_Not_Return_LibraryItems()
            {
                // Setup 
                var bogusSearch = "Bla";
                var expectedLibraryItems = LibraryContext.Object.LibraryItems.Where(li => li.Name.Contains(bogusSearch, StringComparison.InvariantCultureIgnoreCase)).ToArray();

                // Get result
                var res = await LibraryRepository.SearchLibraryItemsAsync(bogusSearch);

                // Asset
                Assert.Empty(res);
            }
        }

        // Test Read All
        public class ReadAllAsync : LibraryRepositoryTests
        {
            [Fact]
            public async void Should_Return_All_LibraryItems()
            {
                // Get result
                var res = await LibraryRepository.ReadAllLibaryItemsAsync();
                var libaryItems = LibraryContext.Object.LibraryItems.ToArray();

                // Assert
                Assert.Collection(res, 
                    li => Assert.Same(libaryItems[0], li),
                    li => Assert.Same(libaryItems[1], li),
                    li => Assert.Same(libaryItems[2], li)
                );
            }
        }

        // Test Update
        public class UpdateAsync : LibraryRepositoryTests
        {
            [Fact]
            public async void Should_Update_LibraryItem()
            {
                // Setup
                var libaryItems = LibraryContext.Object.LibraryItems.ToArray();
                var expectedLibraryItem = new LibraryItem { LibraryItemKey = libaryItems[0].LibraryItemKey, Name = "New Test LibraryItem" };
                

                // Get result
                var res = await LibraryRepository.UpdateLibraryItemAsync(expectedLibraryItem);

                // Assert
                Assert.Same(expectedLibraryItem, res);
                LibraryContext.Verify(lc => lc.SaveChanges(), Times.Once);
            }
        }

        // Delete
        public class DeleteAsync : LibraryRepositoryTests
        {
            [Fact]
            public async void Should_Delete_LibraryItem()
            {
                // Setup
                var libraryItems = LibraryContext.Object.LibraryItems.ToArray();
                var expectedLibraryItem = libraryItems[0];

                // Get result
                var res = await LibraryRepository.DeleteLibraryItemAsync(expectedLibraryItem.LibraryItemKey);

                // Assert
                Assert.Same(expectedLibraryItem, res);
            }
        }
    }
}
