﻿using DS_Model;
using Microsoft.EntityFrameworkCore;

namespace DS_Contexts
{
    public class LibraryContext : DbContext
    {
        public LibraryContext(DbContextOptions<LibraryContext> options) : base(options)
        {
        }

        public LibraryContext() { }

        public virtual DbSet<LibraryItem> LibraryItems { get; set; }
    }
}
