﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DS_Model
{
    public class LibraryItem
    {
        private Guid _libraryItemKey;
        [Key]
        public Guid LibraryItemKey
        {
            get
            {
                if (_libraryItemKey == default)
                {
                    _libraryItemKey = Guid.NewGuid();
                }

                return _libraryItemKey;
            }

            set
            {
                _libraryItemKey = value;
            }
        }

        public string Name { get; set; }
    }
}
