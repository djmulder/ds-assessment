﻿using DS_Contexts;
using DS_Interfaces;
using DS_Repositories;
using DS_Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DS_WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<LibraryContext>(o => o.UseSqlServer(Configuration.GetConnectionString("AssessmentDatabase")));
            services.AddTransient<ILibraryRepository, LibraryRepository>();
            services.AddTransient<ILibraryService, LibraryService>();

            // Had to set cors as there are 2 different servers physically (one on 1337 and one on 43215)
            services.AddCors(o => o.AddPolicy("AllowLocal", b => b.AllowAnyHeader().AllowAnyMethod().WithOrigins("http://localhost:1337")));
            services.AddMvc();
            services.Configure<MvcOptions>(o => o.Filters.Add(new CorsAuthorizationFilterFactory("AllowLocal")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            
            app.UseMvc();
            app.UseCors("AllowLocal");
        }
    }
}
