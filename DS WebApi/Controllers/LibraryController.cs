﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DS_Model;
using DS_Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace DS_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LibraryController : ControllerBase
    {
        private ILibraryService LibraryService { get; }

        public LibraryController(ILibraryService libraryService)
        {
            LibraryService = libraryService ?? throw new ArgumentNullException();
        }

        // GET: api/Library
        [HttpGet]
        public async Task<IEnumerable<LibraryItem>> Get()
        {
            return await LibraryService.ReadAllLibraryItems();
        }

        // GET: api/Library/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<string> Get(Guid id)
        {
            return (await LibraryService.ReadOneLibraryItemAsync(id)).Name;
        }

        // GET: api/searchLibrary/search
        [HttpGet("search/{search}")]
        public async Task<ActionResult> Search(string search)
        {
            var res = await LibraryService.SearchLibrayItemsAsync(search);
            if (res.Any())
            {
                return new OkResult();
            }
            else
            {
                return new NotFoundResult();
            }
        }

        // POST: api/Library
        [HttpPost]
        public async void Post([FromBody] LibraryItem libraryItem)
        {
            var newLibraryItem = new LibraryItem { Name = libraryItem.Name };
            await LibraryService.CreateLibraryItemAsync(newLibraryItem);
        }

        // PUT: api/Library/5
        [HttpPut("{id}")]
        public async void Put([FromRoute]Guid id, [FromBody] LibraryItem libraryItem)
        {
            var updatedItem = await LibraryService.ReadOneLibraryItemAsync(libraryItem.LibraryItemKey);
            updatedItem.Name = libraryItem.Name;
            await LibraryService.UpdateLibraryItemAsync(updatedItem);
        }

        // DELETE: api/Library/5
        [HttpDelete("{id}")]
        public async void Delete(Guid id)
        {
            await LibraryService.DeleteLibraryItemAsync(id);
        }
    }
}
