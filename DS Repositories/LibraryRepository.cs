﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DS_Contexts;
using DS_Interfaces;
using DS_Model;

namespace DS_Repositories
{
    public class LibraryRepository : ILibraryRepository
    {
        private LibraryContext LibraryContext { get; set; }

        public LibraryRepository(LibraryContext libraryContext)
        {
            LibraryContext = libraryContext ?? throw new ArgumentNullException();

            LibraryContext.Database?.EnsureCreated();
        }

        // Create LibraryItem
        public Task<LibraryItem> CreateLibraryItemAsync(LibraryItem libraryItem)
        {
            LibraryContext.Add(libraryItem);
            LibraryContext.SaveChanges();
            return Task.FromResult(libraryItem);
        }

        // Delete LibraryItem
        public Task<LibraryItem> DeleteLibraryItemAsync(Guid key)
        {
            var toDeleteItem = LibraryContext.LibraryItems.FirstOrDefault(li => li.LibraryItemKey == key);

            if (toDeleteItem == default)
            {
                return null;
            }

            LibraryContext.Remove(toDeleteItem);
            LibraryContext.SaveChanges();

            return Task.FromResult(toDeleteItem);
        }
        
        // Read All LibraryItems
        public Task<IEnumerable<LibraryItem>> ReadAllLibaryItemsAsync()
        {
            return Task.FromResult(LibraryContext.LibraryItems.AsEnumerable());
        }

        // Read One LibraryItem
        public Task<LibraryItem> ReadOneLibraryItemAsync(Guid key)
        {
            return Task.FromResult(LibraryContext.LibraryItems.FirstOrDefault(li => li.LibraryItemKey == key));
        }

        // Update LibraryItem
        public Task<LibraryItem> UpdateLibraryItemAsync(LibraryItem libraryItem)
        {
            var shouldUpdateItem = LibraryContext.LibraryItems.Any(li => li.LibraryItemKey == libraryItem.LibraryItemKey);

            if (!shouldUpdateItem)
            {
                return null;
            }

            LibraryContext.Update(libraryItem);
            LibraryContext.SaveChanges();

            return Task.FromResult(libraryItem);
        }

        // Search LibraryItem
        public Task<IEnumerable<LibraryItem>> SearchLibraryItemsAsync(string search)
        {
            var res = LibraryContext.LibraryItems.Where(li => li.Name.Contains(search, StringComparison.InvariantCultureIgnoreCase)).AsEnumerable();

            return Task.FromResult(res);
        }
    }
}
