﻿interface IHandledWord {
    word: string;
    isFound: boolean;
}

class HandledWord implements IHandledWord {
    public word: string;
    public isFound: boolean;

    constructor(handledWord: IHandledWord) {
        if (handledWord == null) {
            return;
        }
        this.word = handledWord.word;
        this.isFound = handledWord.isFound;
    }
}