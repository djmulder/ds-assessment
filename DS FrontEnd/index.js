angular.module("LibraryItemApp", ["ngRoute"])
    .controller("LibraryItemController", LibraryItemsViewModel)
    .config(($routeProvider) => {
    $routeProvider.when("/", {
        templateUrl: "libraryItems.html", controller: "LibraryItemController"
    });
});
//# sourceMappingURL=index.js.map