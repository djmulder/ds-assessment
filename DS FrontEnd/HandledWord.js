class HandledWord {
    constructor(handledWord) {
        if (handledWord == null) {
            return;
        }
        this.word = handledWord.word;
        this.isFound = handledWord.isFound;
    }
}
//# sourceMappingURL=HandledWord.js.map