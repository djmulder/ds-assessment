﻿import express = require('express');
var bodyParser = require('body-parser');

var app = express();
app.use(bodyParser());
app.use(express.static("."));
app.listen(process.env.PORT || 1337)