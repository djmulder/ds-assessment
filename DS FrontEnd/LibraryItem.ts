﻿interface ILibraryItem {
    libraryItemKey: string
    name: string;
}

class LibraryItem implements ILibraryItem {
    public libraryItemKey: string;
    public name: string;

    constructor(libraryItem: ILibraryItem) {
        this.libraryItemKey = libraryItem.libraryItemKey;
        this.name = libraryItem.name;
    }
}