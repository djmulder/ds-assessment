﻿angular.module("LibraryItemApp", ["ngRoute"])
    .controller("LibraryItemController", LibraryItemsViewModel)
    .config(($routeProvider: ng.route.IRouteProvider) => {
        $routeProvider.when("/", {
            templateUrl: "libraryItems.html", controller: "LibraryItemController"
        });
    });