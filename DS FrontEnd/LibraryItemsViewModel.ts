﻿interface ILibraryItemsViewModel extends ng.IScope {
    libraryItems: Array<ILibraryItem>;
    handledWords: Array<IHandledWord>;
    currentItem: string;
    currentSelectedItem: string;
    refresh: () => void;
    remove: (index: string) => void;
    startUpdate: (index: string) => void;
    save: () => void;
    search: (element: HTMLDivElement) => void;
}

class LibraryItemsViewModel {
    constructor($scope: ILibraryItemsViewModel, $http: ng.IHttpService) {
        $scope.libraryItems = new Array<ILibraryItem>();
        $scope.handledWords = new Array<IHandledWord>();

        $scope.refresh = () => {
            // get libraryitems from the rest interface and apply them to the viewmodel
            $http.get<Array<ILibraryItem>>("http://localhost:43215/api/library")
                .then(LibraryItemResponse => {
                    $scope.libraryItems = new Array<ILibraryItem>();
                    LibraryItemResponse.data.forEach(li => {
                        // update if the index is found otherwise add
                        var itemKey = li.libraryItemKey;

                        var index = $scope.libraryItems.findIndex(item => itemKey == item.libraryItemKey);
                        if (index > -1) {
                            $scope.libraryItems[index] = li;
                        }
                        else {
                            $scope.libraryItems.push(li);
                        }
                    });
                });
        }

        $scope.remove = (index: string) => {
            $http.delete("http://localhost:43215/api/library/" + index).then(() => $scope.refresh());
        }

        $scope.startUpdate = (index: string) => {
            $scope.currentSelectedItem = index;
            $scope.currentItem = $scope.libraryItems.find(item => index == item.libraryItemKey).name;
        }

        $scope.save = () => {
            if ($scope.currentItem == "") {
                return; // TODO: error handling
            }

            // if an item isn't selected add new
            if ($scope.currentSelectedItem == "") {
                $http.post("http://localhost:43215/api/library/", { Name: $scope.currentItem }).then(() => $scope.refresh());
            }
            // if an item is selected update selected item
            else {
                $http.put("http://localhost:43215/api/library/" + $scope.currentSelectedItem, { LibraryItemKey: $scope.currentSelectedItem, Name: $scope.currentItem }).then(() => $scope.refresh());
            }

            $scope.currentSelectedItem = "";
            $scope.currentItem = "";
        }

        $scope.search = (element: HTMLDivElement) => {
            // search all words that are at least 3 characters long
            var words = element.innerText.match(/\w{3,}/gi);
            var wordsToSearch = new Array<string>();

            if (words == null) {
                return;
            }

            // filter result on already handled words
            words.forEach((word) => {
                var item = $scope.handledWords.find(w => w.word == word);
                if (item == null) {
                    wordsToSearch.push(word);
                }
                else if (item.isFound) {
                    this.underlineSearchValue(item.word, element);
                }
            });

            // underline earch word that's found and push searched words to the handled words
            wordsToSearch.forEach((word) => {
                var handledWord = new HandledWord(null);
                handledWord.isFound = false;
                handledWord.word = word;
                $http.get("http://localhost:43215/api/library/search/" + word).then(() => {
                    handledWord.isFound = true;
                    this.underlineSearchValue(word, element);
                })
                    .finally(() => {
                        $scope.handledWords.push(handledWord);
                    });
            });
        }
    }

    private getCaret(element: HTMLDivElement) {
        var range = window.getSelection().getRangeAt(0);
        var preCaretRange = range.cloneRange();
        preCaretRange.selectNodeContents(element);
        preCaretRange.setEnd(range.endContainer, range.endOffset);
        var caretOffset = preCaretRange.toString().length;
        return caretOffset;
    }

    private setCaret(element: HTMLDivElement, pos: number) {
        var charIndex = 0, range = document.createRange();
        range.setStart(element, 0);
        range.collapse(true);
        var nodeStack = [element], node, foundStart = false, stop = false;

        while (!stop && (node = nodeStack.pop())) {
            if (node.nodeType == 3) {
                var nextCharIndex = charIndex + node.length;
                if (!foundStart && pos >= charIndex && pos <= nextCharIndex) {
                    range.setStart(node, pos - charIndex);
                    foundStart = true;
                }
                if (foundStart && pos >= charIndex && pos <= nextCharIndex) {
                    range.setEnd(node, pos - charIndex);
                    stop = true;
                }
                charIndex = nextCharIndex;
            } else {
                var i = node.childNodes.length;
                while (i--) {
                    nodeStack.push(node.childNodes[i]);
                }
            }
        }
        var selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
    }

    private underlineSearchValue(wordToSearchFor: string, element: HTMLDivElement) {
        var oldCaret = this.getCaret(element);
        element.innerHTML = element.innerText.replace(new RegExp(wordToSearchFor, 'g'), "<u>" + wordToSearchFor + "</u>");
        this.setCaret(element, oldCaret);
    }
}