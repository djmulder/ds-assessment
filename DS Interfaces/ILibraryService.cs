﻿using DS_Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DS_Interfaces
{
    public interface ILibraryService
    {
        Task<IEnumerable<LibraryItem>> SearchLibrayItemsAsync(string search);
        Task<LibraryItem> CreateLibraryItemAsync(LibraryItem librayItem);
        Task<LibraryItem> ReadOneLibraryItemAsync(Guid libraryItemKey);
        Task<IEnumerable<LibraryItem>> ReadAllLibraryItems();
        Task<LibraryItem> UpdateLibraryItemAsync(LibraryItem libraryItem);
        Task<LibraryItem> DeleteLibraryItemAsync(Guid libraryKey);
    }
}
