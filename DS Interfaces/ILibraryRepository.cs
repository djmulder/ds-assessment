﻿using DS_Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DS_Interfaces
{
    public interface ILibraryRepository
    {
        Task<IEnumerable<LibraryItem>> ReadAllLibaryItemsAsync();
        Task<IEnumerable<LibraryItem>> SearchLibraryItemsAsync(string search);
        Task<LibraryItem> ReadOneLibraryItemAsync(Guid key);
        Task<LibraryItem> CreateLibraryItemAsync(LibraryItem libraryItem);
        Task<LibraryItem> UpdateLibraryItemAsync(LibraryItem libraryItem);
        Task<LibraryItem> DeleteLibraryItemAsync(Guid key);
    }
}
